const fs = require("fs");
const {VIDEO_PER_FRAME, SCREEN_BUFFER, KEY_FRAME, REG_PAGE, PAGE1, PAGE2} = require("./constants");

function shuffle(array) {
	array.sort((a, b) => Math.random() - 0.5);
}

module.exports = imgs => {
	let frames = [];

	let currentScreen = [];
	for(let i = 0; i < 256 * 256 / 8; i++) {
		currentScreen.push(0);
	}
	let currentScreenKF = [];
	for(let i = 0; i < 256 * 256 / 8; i++) {
		currentScreenKF.push(0);
	}

	let cur;

	let page = 0o40000;
	let keyframeCnt = 0;


	// Force key-frames
	let wordsChanged = [];
	let keyFrameWordsChanged = [];
	let forceKeyFrame = {};
	let curPalette = -1;
	for(let i = 0; i < imgs.length; i++) {
		process.stderr.write(`Forcing key-frames (${i+1} done)\n`);
		cur = null;
		cur = fs.readFileSync("apple-raw/" + imgs[i]);

		let imgPalette = cur[cur.length - 1];

		for(let offset = 0; offset < 256 * 256 / 8; offset++) {
			const value = cur.readUInt16LE(offset * 2);

			const idx = wordsChanged.indexOf(offset);
			if(currentScreen[offset] != value && idx == -1) {
				wordsChanged.push(offset);
			}
			if(currentScreen[offset] == value && idx != -1) {
				wordsChanged.splice(idx, 1);
			}

			const idx2 = keyFrameWordsChanged.indexOf(offset);
			if(currentScreenKF[offset] != value && idx2 == -1) {
				keyFrameWordsChanged.push(offset);
			}
			if(currentScreenKF[offset] == value && idx2 != -1) {
				keyFrameWordsChanged.splice(idx2, 1);
			}
		}

		let priority = keyFrameWordsChanged.length;
		const needToChangePalette = imgPalette != curPalette && curPalette != -1;
		if(needToChangePalette) {
			priority = Infinity;
		}

		if(keyFrameWordsChanged.length > KEY_FRAME || needToChangePalette) {
			// Check that we don't break any keyframes before
			let need = 0o24000; // Just in case
			let isKeyFrame = true;
			let replaced = [];
			for(let j = i - 1; j >= 0 && need > 0; j--) {
				if(forceKeyFrame[j] && forceKeyFrame[j] > priority) {
					// Oops
					isKeyFrame = false;
					break;
				}
				if(forceKeyFrame[j]) {
					replaced.push(j);
				}
				need -= VIDEO_PER_FRAME - frames[j].length;
			}

			if(replaced.length) {
				console.log(
					`This frame (${i}, priority ${priority}) replaces keyframe(s) ` +
					replaced.map(frame => {
						return `${frame} (priority ${forceKeyFrame[frame]})`;
					}).join(", ")
				);
			}

			if(isKeyFrame) {
				// Mark this frame as force-key-frame
				forceKeyFrame[i] = priority;

				// We'll make this frame key-frame
				keyFrameWordsChanged.length = 0;
				for(let j = 0; j < currentScreenKF.length; j++) {
					currentScreenKF[j] = cur.readUInt16LE(j * 2);
				}

				// Now fill frames before current with changes
				need = 0o24000; // Just in case
				for(let j = i - 1; j >= 0 && need > 0; j--) {
					forceKeyFrame[j] = 0;
					need -= VIDEO_PER_FRAME - frames[j].length;
				}

				curPalette = imgPalette;
			}
		}

		shuffle(wordsChanged);

		let frame = [];
		wordsChanged.splice(0, VIDEO_PER_FRAME - SCREEN_BUFFER).forEach((word, i) => {
			currentScreen[word] = cur.readUInt16LE(word * 2);
			frame.push([currentScreen[word], page + word * 2]);
		});
		frames.push(frame);
	}



	currentScreen.length = 0;
	for(let i = 0; i < 256 * 256 / 8; i++) {
		currentScreen.push(0);
	}

	// Diff
	wordsChanged.length = 0;
	frames.length = 0;
	curPalette = -1;
	for(let i = 0; i < imgs.length; i++) {
		process.stderr.write(`Encoding (${i+1} done)\n`);
		cur = null;
		cur = fs.readFileSync("apple-raw/" + imgs[i]);

		let imgPalette = cur[cur.length - 1];

		for(let offset = 0; offset < 256 * 256 / 8; offset++) {
			const value = cur.readUInt16LE(offset * 2);

			const idx = wordsChanged.indexOf(offset);
			if(currentScreen[offset] != value && idx == -1) {
				wordsChanged.push(offset);
			}
			if(currentScreen[offset] == value && idx != -1) {
				wordsChanged.splice(idx, 1);
			}
		}

		shuffle(wordsChanged);

		if(forceKeyFrame[i]) {
			// It's quite possible that we have some free space somewhere
			// in previous frames, so we can use another page.
			let frameSince = 0;
			for(let j = 0; j < frames.length; j++) {
				if(frames[j].find(write => write[1] == REG_PAGE)) {
					frameSince = j;
				}
			}
			let free = 0;
			frames.slice(frameSince).forEach(prevFrame => {
				free += VIDEO_PER_FRAME - prevFrame.length;
			});

			let keyframe = [];
			let newPage = 0o140000 - page;
			for(let offset = 0; offset < 256 * 256 / 8; offset++) {
				const value = cur.readUInt16LE(offset * 2);
				keyframe.push([value, newPage + offset * 2]);
			}

			if(free >= keyframe.length) {
				// We have free space
				process.stderr.write(`Keyframe #${keyframeCnt++}\n`);

				for(let j = frameSince; j < frames.length && keyframe.length; j++) {
					while(frames[j].length < VIDEO_PER_FRAME && keyframe.length) {
						const [value, address] = keyframe.pop();
						frames[j].push([value, address]);
					}
				}


				wordsChanged.length = 0;
				for(let j = 0; j < currentScreen.length; j++) {
					currentScreen[j] = cur.readUInt16LE(j * 2);
				}
				if(newPage == 0o40000) {
					frames.push([[PAGE1 + imgPalette * 0o400, REG_PAGE]]);
				} else {
					frames.push([[PAGE2 + imgPalette * 0o400, REG_PAGE]]);
				}

				curPalette = imgPalette;
				page = newPage;
				continue;
			} else {
				if(forceKeyFrame[i]) {
					process.stderr.write(`Forced keyframe cannot be a keyframe: expected at least ${keyframe.length} space, got ${free}`);
				}
			}
		}


		let frame = [];
		wordsChanged.splice(0, VIDEO_PER_FRAME - SCREEN_BUFFER).forEach((word, i) => {
			currentScreen[word] = cur.readUInt16LE(word * 2);
			frame.push([currentScreen[word], page + word * 2]);
		});
		frames.push(frame);
	}

	let firstImg = fs.readFileSync("apple-raw/" + imgs[0]);
	let firstPalette = firstImg[firstImg.length - 1];

	return [frames, firstPalette];
};
const fs = require("fs");
const {FRAME_COUNT} = require("./constants");

let imgs = fs.readdirSync("apple-raw");
imgs.sort();
if(FRAME_COUNT != Infinity) {
	imgs = imgs.slice(0, FRAME_COUNT);
}

console.log("Generating");
let [frames, firstPalette] = require("./gen_module")(imgs);
console.log("First palette:", firstPalette);
console.log("Saving");
fs.writeFileSync("frames.json", JSON.stringify(frames));
fs.writeFileSync("first-palette.txt", firstPalette.toString());
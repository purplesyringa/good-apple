const fs = require("fs");
const PNG = require("pngjs").PNG;

function parse(file) {
	return new Promise(resolve => {
		fs.createReadStream(file)
			.pipe(new PNG({
				filterType: 4
			}))
			.on("parsed", function() {
				resolve(this);
			});
	});
}

let imgs = fs.readdirSync("apple");
imgs.sort();

const frames = [];
let cnt = imgs.length;
Promise.all(imgs.map(path => {
	return parse("apple/" + path)
		.then(img => {
			cnt--;
			process.stderr.write(cnt + "\n");
			frames[parseInt(path.replace(/\D/g, ""), 10)] = img;
		});
}))
	.then(() => {
		for(let i = 1; i < frames.length; i++) {
			process.stderr.write(`Testing (${i} done)\n`);
			const prev = frames[i - 1];
			const cur = frames[i];

			let wordsChanged = 0;
			for(let y = 0; y < 256; y++) {
				let wordChanged = false;

				for(let x = 0; x < 256; x++) {
					const offset = (y * 256 + x) * 4;

					const [r1, g1, b1] = prev.data.slice(offset, offset + 3);
					const [r2, g2, b2] = cur .data.slice(offset, offset + 3);
					const rd = Math.round((r1 - r2) / 256),
					      gd = Math.round((g1 - g2) / 256),
					      bd = Math.round((b1 - b2) / 256);

					if(rd || gd || bd) {
						wordChanged = true;
					}

					if(wordChanged && x % 8 == 7) {
						wordChanged = false;
						wordsChanged++;
					}
				}
			}

			let percentChanged = wordsChanged / (256 * 256 / 8);
			let color = percentChanged > 0.5 ? "red" : percentChanged > 0.25 ? "yellow" : percentChanged > 0.1 ? "green" : "gray";
			process.stdout.write(`<div style='display: inline-block; height: ${percentChanged * 512}px; width: 1px; background-color: ${color};'></div>`);
		}
	});
const fs = require("fs");
const PNG = require("pngjs").PNG;
const {PALETTES, BCI_EMPTY_LINES_SAFE} = require("./constants");

const file = process.argv[2];
if(!file) {
	process.stdout.write("\n");
	process.stdout.write("                ##########################\n");
	process.stdout.write("                # CompactImage Converter #\n");
	process.stdout.write("                #  for GoodApple!! demo  #\n");
	process.stdout.write("                #       on BK0011M       #\n");
	process.stdout.write("                #    by Ivanq / Sands    #\n");
	process.stdout.write("                ##########################\n");
	process.stdout.write("\n");
	process.stdout.write("Usage:\n");
	process.stdout.write("node compact-image-converter image.png\n");
	process.stdout.write("Converts image.png to image.bci and image.pal.bci\n");
	process.exit(0);
}

// BCI format:
// 1 word -- word count to skip from left
// 1 word -- length (A)
// A words -- image data
// next line; repeat

// PAL.BCI format:
// HEIGHT words -- 0o100000 | (palette << 8)

const png = PNG.sync.read(fs.readFileSync(file));

// Find all palettes that can be used on each line
let paletteIdsByLine = [];
let isLineEmpty = [];
for(let y = 0; y < png.height; y++) {
	let allColors = [];
	for(let x = 0; x < png.width; x++) {
		const offset = (y * png.width + x) * 4;
		const [r, g, b] = png.data.slice(offset, offset + 3);
		const hex = colorToHex(r, g, b);
		if(allColors.indexOf(hex) === -1) {
			allColors.push(hex);
		}
	}

	let paletteIds = PALETTES.filter(palette => {
		return allColors.every(color => palette.indexOf(color) > -1);
	}).map(palette => PALETTES.indexOf(palette));
	if(paletteIds.length === 0) {
		process.stderr.write("ERR!\n");
		process.stderr.write(`Palette ${allColors.join(", ")} not found (at y = ${y}).\n`);
		process.exit(1);
	}

	paletteIdsByLine.push(paletteIds);
	isLineEmpty.push(allColors.length === 1 && allColors[0] === "#000000");
}

// Split by empty lines (2 or more)
let emptyLinesInARow = 0;
let start = 0;
let marginTop = 0;
let safeParts = [];
for(let y = 0; y < png.height; y++) {
	if(isLineEmpty[y]) {
		emptyLinesInARow++;
	} else {
		if(emptyLinesInARow >= BCI_EMPTY_LINES_SAFE) {
			// If the above lines were safe to switch,
			// note this.
			safeParts.push({
				from: start,
				to: y - emptyLinesInARow,
				marginTop,
				marginBottom: Math.floor(emptyLinesInARow / 2)
			});

			start = y;
			marginTop = Math.ceil(emptyLinesInARow / 2);
		}
		emptyLinesInARow = 0;
	}
}

safeParts.push({
	from: start,
	to: png.height,
	marginTop,
	marginBottom: 0
});


// For each safe part: find palette
let paletteOf = [];
for(let y = 0; y < png.height; y++) {
	paletteOf.push(null);
}

for(const safePart of safeParts) {
	let allPaletteIds = [];
	for(let i = 0; i < 16; i++) {
		allPaletteIds.push(i);
	}

	for(let y = safePart.from; y < safePart.to; y++) {
		let paletteIds = paletteIdsByLine[y];

		allPaletteIds.slice().forEach(paletteId => {
			if(paletteIds.indexOf(paletteId) === -1) {
				// If this palette could be used above this line,
				// but can't be used now -- remove it.
				allPaletteIds.splice(allPaletteIds.indexOf(paletteId), 1);
			}
		});
	}

	// Is there at least 1 palette that can be used in all the lines?
	if(allPaletteIds.length >= 1) {
		// Use it.
		const from = safePart.from - safePart.marginTop;
		const to = safePart.to + safePart.marginBottom;
		for(let y = from; y < to; y++) {
			paletteOf[y] = allPaletteIds[0];
		}
	} else {
		process.stderr.write("Huston, we have a problem.\n");
		process.stderr.write("There is no palette that can fill all\n");
		process.stderr.write(`lines from ${safePart.from} to ${safePart.to}.\n`);
		process.stderr.write("Falling back to unsafe fill.\n");
		process.stderr.write("\n");

		// Part itself
		for(let y = 0; y < safePart.length; y++) {
			paletteOf[safePart.from + y] = paletteIdsByLine[safePart.from + y];
		}

		// Top margin
		const firstPalette = paletteIdsByLine[safePart.from];
		for(let y = 0; y < safePart.topMargin; y++) {
			paletteOf[safePart.from - safePart.topMargin + y] = firstPalette;
		}

		// Bottom margin
		const lastPalette = paletteIdsByLine[safePart.to - 1];
		for(let y = 0; y < safePart.bottomMargin; y++) {
			paletteOf[safePart.to + y] = lastPalette;
		}
	}
}


let out = fs.createWriteStream(file.replace(/\.png$/i, "") + ".bci");
let palOut = fs.createWriteStream(file.replace(/\.png$/i, "") + ".pal.bci");
for(let y = 0; y < png.height; y++) {
	const paletteId = paletteOf[y];
	if(paletteId === null) {
		process.stderr.write(`No palette mapped to line ${y}.\n`);
		process.exit(1);
	}

	// Pixel to 2-bit color
	let pixels = [];
	for(let x = 0; x < png.width; x++) {
		const offset = (y * png.width + x) * 4;
		const [r, g, b] = png.data.slice(offset, offset + 3);
		const bkColor = PALETTES[paletteId].indexOf(colorToHex(r, g, b));

		pixels.push(bkColor);
	}

	// 2-bit-color list of pixels to words
	let words = [];
	for(let x = 0; x < pixels.length; x += 8) {
		words.push(
			((pixels[x + 0] || 0) <<  0) |
			((pixels[x + 1] || 0) <<  2) |
			((pixels[x + 2] || 0) <<  4) |
			((pixels[x + 3] || 0) <<  6) |
			((pixels[x + 4] || 0) <<  8) |
			((pixels[x + 5] || 0) << 10) |
			((pixels[x + 6] || 0) << 12) |
			((pixels[x + 7] || 0) << 14)
		);
	}

	// Get zero word count
	let zeroWords = 0;
	while(words[0] === 0) {
		zeroWords++;
		words.shift();
	}

	// Unpad
	while(words[words.length - 1] === 0) {
		words.pop();
	}

	write(zeroWords);
	write(words.length);
	for(let i = 0; i < words.length; i++) {
		write(words[i]);
	}

	palWrite(0o100000 | (paletteId << 8));
}


function write(uint16) {
	let buf = Buffer.alloc(2);
	buf.writeUInt16LE(uint16);
	out.write(buf);
}

function palWrite(uint16) {
	let buf = Buffer.alloc(2);
	buf.writeUInt16LE(uint16);
	palOut.write(buf);
}

function colorToHex(r, g, b) {
	return "#" + hex(r) + hex(g) + hex(b);
}
function hex(n) {
	n = n.toString(16).toUpperCase();
	return "0".repeat(2 - n.length) + n;
}
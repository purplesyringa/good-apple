const fs = require("fs");
const PNG = require("pngjs").PNG;
const {PALETTES} = require("./constants");

function parse(file) {
	return PNG.sync.read(fs.readFileSync(file));
}

function getWord(wordId, frame, palette) {
	let word = 0;

	for(let i = 0; i < 8; i++) {
		let pos = (wordId * 8 + i) * 4;
		let [r, g, b] = frame.data.slice(pos, pos + 3);
		word |= getColor(r, g, b, palette) << (i * 2);
	}

	return word;
}

function getColor(r, g, b, palette) {
	if(`${r},${g},${b}` == palette[1].join(",")) {
		return 1;
	} else if(`${r},${g},${b}` == palette[2].join(",")) {
		return 2;
	} else if(`${r},${g},${b}` == palette[3].join(",")) {
		return 3;
	} else {
		return 0;
	}
}

function getPalette(frame) {
	let offset = (256 * 260 + 20) * 4;
	const color0 = frame.data.slice(offset, offset + 3);

	offset += 256 / 4 * 4;
	const color1 = frame.data.slice(offset, offset + 3);

	offset += 256 / 4 * 4;
	const color2 = frame.data.slice(offset, offset + 3);

	offset += 256 / 4 * 4;
	const color3 = frame.data.slice(offset, offset + 3);

	const color0hex = "#" + hex(color0[0]) + hex(color0[1]) + hex(color0[2]);
	const color1hex = "#" + hex(color1[0]) + hex(color1[1]) + hex(color1[2]);
	const color2hex = "#" + hex(color2[0]) + hex(color2[1]) + hex(color2[2]);
	const color3hex = "#" + hex(color3[0]) + hex(color3[1]) + hex(color3[2]);

	const id = PALETTES
		.findIndex(palette => {
			return (
				palette.indexOf(color0hex) != -1 &&
				palette.indexOf(color1hex) != -1 &&
				palette.indexOf(color2hex) != -1 &&
				palette.indexOf(color3hex) != -1
			);
		});

	if(id == -1) {
		process.stderr.write(`Unknown palette: ${color0hex}, ${color1hex}, ${color2hex}, ${color3hex}`);
		process.exit(1);
	}

	return [
		PALETTES[id].map(color => {
			return [
				parseInt(color.substr(1, 2), 16),
				parseInt(color.substr(3, 2), 16),
				parseInt(color.substr(5, 2), 16)
			];
		}),
		id
	];
}
function hex(n) {
	n = n.toString(16).toUpperCase();
	return "0".repeat(2 - n.length) + n;
}


let imgs = fs.readdirSync("apple");

for(let i = 0; i < imgs.length; i++) {
	process.stderr.write(`Encoding (${i+1} done)\n`);

	let img = parse("apple/" + imgs[i]);

	let [palette, paletteId] = getPalette(img);

	let rawImg = Buffer.alloc(256 * 256 / 8 * 2 + 1);
	for(let offset = 0; offset < 256 * 256 / 8; offset++) {
		const value = getWord(offset, img, palette);
		rawImg.writeUInt16LE(value, offset * 2);
	}
	rawImg[rawImg.length - 1] = paletteId;

	fs.writeFileSync("apple-raw/" + imgs[i].replace(/\.png$/i, ".raw"), rawImg);
}